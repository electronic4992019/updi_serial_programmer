# UPDI SERIAL PROGRAMMER

utility to program an ATMEGA 4808 and other ATMEL MCUs with the SerialUPDI interface

based on https://github.com/wagiminator/AVR-Programmer/blob/master/SerialUPDI_Programmer/

see also here for UPDI explained : https://github.com/SpenceKonde/AVR-Guidance/blob/master/UPDI/jtag2updi.md#

